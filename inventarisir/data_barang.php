<?php
include "../koneksi.php";
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>INVENTARIS</title>
    <link href="../inventarisir/assets/css/bootstrap.css" rel="stylesheet" />
    <link href="../inventarisir/assets/css/font-awesome.css" rel="stylesheet" />
    <link href="../inventarisir/assets/css/custom.css" rel="stylesheet" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <link href="../inventarisir/assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    <link href="../inventarisir/assets/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
    <link href="../inventarisir/assets/datatables/dataTables.bootstrap4.js" rel="stylesheet"/>
    <link href="../inventarisir/assets/datatables/jquery.dataTables.js" rel="stylesheet"/>
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="barang.php"><font size="3px">INVENSS</font></a> 
            </div>
  
        </nav>   
           <!-- /. NAV TOP  -->
                <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                <li class="text-center">
                    <img src="assets/img/adm.png" class="user-image img-responsive"/>
                    <h2></h2>
                    </li>

                    
                     <li>
                        <a class=""  href=""><i class="fa fa-home fa-fw nav_icon"></i>Inventarisir</a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="../inventarisir/data_barang.php">Data Barang</a>
                            </li>
                            <li>
                                <a href="../inventarisir/jenis.php">Jenis</a>
                            </li>
                            <li>
                                <a href="../inventarisir/ruang.php">Ruang</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="" href="../peminjaman/peminjam.php"><i class="fa fa-cloud-download fa-fw nav_icon"></i>Peminjaman</a>
                    </li>
                    <li>
                        <a class="" href="../pengembalian/kembali.php"><i class="fa fa-cloud-upload fa-fw nav_icon"></i>Pengembalian</a>
                    </li>
                     <li>
                        <a class=""  href=""><i class="fa fa-file fa-fw nav_icon"></i>Laporan</a>
                        <ul class="nav nav-second-level">
                            <li>
                                <a href="">Data Barang</a>
                            </li>
                            <li>
                                <a href="">Peminjaman</a>
                            </li>
                            <li>
                                <a href="">Pengembalian</a>
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a class="" href="../inventarisir/backup_db.php"><i class="fa fa-folder-open-o fa-fw nav_icon"></i>Backup Database</a>
                    </li>
                    <li>
                        <a class="" href="../logout.php"><i class="fa fa fa-fw nav_icon"></i>Logout</a>
                    </li>
                </ul>
               
            </div>
            
        </nav> 
        <div id="page-wrapper">
    <div class="graphs">
        <div class="row">
        <div class="panel panel-default">
        <div class="panel-heading"><b><center>KATEGORI</center></b></div>

    <div class="panel-body">
        <div class="col-lg-12">
        <div class="col-md-4 col-sm-4">          
            <div class="panel panel-back noti-box">
                <center><img width="75%" height="75%" src='../inventarisir/assets/img/1.gif' /></center>
                <div class="text-box" >
                    <center><p class="main-text"><b>RPL</b></p></center>
                    <br>
                    <center><a href="barang.php?jurusan=rpl"><button class="btn btn-primary">Lihat</button></a></center>
                </div>
             </div>
        </div>
                
        <div class="col-md-4 col-sm-4">          
            <div class="panel panel-back noti-box">
                <center><img width="50%" height="50%" src='../inventarisir/assets/img/2.gif' /></center>
                <div class="text-box" >
                    <center><p class="main-text"><b>ANM</b></p></center>
                    <br>
                    <center><a href="barang.php?jurusan=anm"><button class="btn btn-primary">Lihat</button></a></center>
                </div>
             </div>
        </div>

         <div class="col-md-4 col-sm-4">          
            <div class="panel panel-back noti-box">
                <center><img width="70%" height="70%" src='../inventarisir/assets/img/3.gif' /></center>
                <div class="text-box" >
                    <center><p class="main-text"><b>TKR</b></p></center>
                    <br>
                    <center><a href="barang.php?jurusan=tkr"><button class="btn btn-primary">Lihat</button></a></center>
                </div>
             </div>
         </div>

    </div>

    <div class="col-lg-12">

         <div class="col-md-4 col-sm-4">          
            <div class="panel panel-back noti-box">
                <center><img width="75%" height="75%" src='../inventarisir/assets/img/4.webp' /></center>
                <div class="text-box" >
                    <center><p class="main-text"><b>BC</b></p></center>
                    <br>
                    <center><a href="barang.php?jurusan=bc"><button class="btn btn-primary">Lihat</button></a></center>
                </div>
             </div>
        </div>
        
        <div class="col-md-4 col-sm-4">          
            <div class="panel panel-back noti-box">
                <center><img width="65%" height="65%" src='../inventarisir/assets/img/5.gif' /></center>
                <div class="text-box" >
                    <center><p class="main-text"><b>TPL</b></p></center>
                    <br>
                    <center><a href="barang.php?jurusan=tpl"><button class="btn btn-primary">Lihat</button></a></center>
                </div>
             </div>
        </div>
        
        <div class="col-md-4 col-sm-4">          
            <div class="panel panel-back noti-box">
                <center><img width="65%" height="65%" src='../inventarisir/assets/img/6.gif' /></center>
                <div class="text-box" >
                    <center><p class="main-text"><b>Lainnya</b></p></center>
                    <br>
                    <center><a href="barang.php?jurusan=lainnya"><button class="btn btn-primary">Lihat</button></a></center>
                </div>
             </div>
        </div>
    </div>
        </div>

    </div>
    </div>
</div>

                <script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
     <!-- DATA TABLE SCRIPTS -->
    <script src="assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="assets/js/dataTables/dataTables.bootstrap.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
    </script>
         <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>
    
   
</body>
</html>