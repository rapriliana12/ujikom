<?php
include "../koneksi.php";
session_start();
if($_SESSION['nama_level']!='Petugas'){
    header("location:../login/login.php");
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
      <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>INVENTARIS</title>
    <link href="../inventarisir/assets/css/bootstrap.css" rel="stylesheet" />
    <link href="../inventarisir/assets/css/font-awesome.css" rel="stylesheet" />
    <link href="../inventarisir/assets/css/custom.css" rel="stylesheet" />
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css' />
    <link href="../inventarisir/assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    <link href="../inventarisir/assets/datatables/dataTables.bootstrap4.css" rel="stylesheet" />
    <link href="../inventarisir/assets/datatables/dataTables.bootstrap4.js" rel="stylesheet"/>
    <link href="../inventarisir/assets/datatables/jquery.dataTables.js" rel="stylesheet"/>
</head>
<body>
    <div id="wrapper">
        <nav class="navbar navbar-default navbar-cls-top " role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="barang.php"><font size="3px">INVENSS</font></a> 
            </div>
  
        </nav>   
           <!-- /. NAV TOP  -->
                <nav class="navbar-default navbar-side" role="navigation">
            <div class="sidebar-collapse">
                <ul class="nav" id="main-menu">
                <li class="text-center">
                    <img src="assets/img/adm.png" class="user-image img-responsive"/>
                    <h2></h2>
                    </li>

                    <li>
                        <a class="" href="../peminjaman/peminjam.php"><i class="fa fa-cloud-download fa-fw nav_icon"></i>Peminjaman</a>
                    </li>
                    <li>
                        <a class="" href="../pengembalian/kembali.php"><i class="fa fa-cloud-upload fa-fw nav_icon"></i>Pengembalian</a>
                    </li>
                    <li>
                        <a class="" href="../logout.php"><i class="fa fa-log-out fa-fw nav_icon"></i>Logout</a>
                    </li>
                </ul>
               
            </div>
            
        </nav> 
<div id="page-wrapper">
    <div class="graphs">
        <div class="row">
        <div class="panel panel-default">
        <div class="panel-heading"><b><center>Form Peminjam</center></b></div>
        <div class="panel-body">
        <div class="col-lg-12">
            <form action="tambah_pinjam.php" method="POST" enctype="multipart/form-data" >
                <div class="form-group">
                    <label>Nama Barang</label>
                    <select name="nama" class="form-control" required="" />
                        <?php 
                            include"../koneksi.php";
                            $sql=mysqli_query($konek,"SELECT * FROM inventaris");
                            while($data=mysqli_fetch_array($sql)){
                                if ($data['id_inventaris']==$tampil['id_inventaris']) {
                                    $selected="selected";
                                }else{
                                    $selected="";
                                }

                        ?>
                        <option value="<?php echo $data['nama'];?>" <?php echo $selected ?>><?php echo $data['nama'];?></option>
                        <?php 
                        } ?>
                        
                    </select>
                </div>
              
                <div class="form-group">
                    <label>Peminjam</label>
                      <select name="id_pegawai" class="form-control" required="" />
                        <?php 
                            include"../koneksi.php";
                            $sql=mysqli_query($konek,"SELECT * FROM pegawai");
                            while($data=mysqli_fetch_array($sql)){
                                if ($data['id_pegawai']==$tampil['id_pegawai']) {
                                    $selected="selected";
                                }else{
                                    $selected="";
                                }

                        ?>
                        <option value="<?php echo $data['id_pegawai'];?>" <?php echo $selected ?>><?php echo $data['nama_pegawai'];?></option>
                        <?php 
                        } ?>
                        
                    </select>
                </div>
                <div class="form-group">
                        <input type="Submit" name="simpan" value="simpan" class="btn btn-primary" >
                </div>
            </form>
        </div>
        </div>
        </div>
        </div>
        </div>
     <div class="panel panel-default">
        <div class="panel-heading"><b><center>DATA INVENTARIS</center></b></div>
        <div class="panel-body">
        <br>
        <div class="table-responsive">
            <table id="dataTables-example" class="table table-bordered table-hover table-striped">
                <thead>
                    <tr>
                        <td>No</td>
                        <td>Nama Barang</td>
                        <td>Tanggal Pinjam</td>
                        <td>Status</td>
                        <td>Peminjam</td>
                        <td>Aksi</td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $no=1;
                    $pilih=mysqli_query($konek, "SELECT * FROM peminjaman where status_peminjaman = 'Dipinjam' order by id_peminjaman asc");
                    while($data=mysqli_fetch_array($pilih)){
                        $id_pegawai = $data['id_pegawai'];
                        $q_data_pegawai = mysqli_query($konek, "select * from pegawai where id_pegawai ='$id_pegawai'");
                        $data_pegawai= mysqli_fetch_array($q_data_pegawai);
                    ?>
                    <tr>
                        <td><?=$no++; ?></td>
                        <td><?=$data['nama_barang'];?></td>
                        <td><?=$data['tanggal_pinjam'];?></td>
                        <td><?=$data['status_peminjaman'];?></td>
                        <td><?=$data_pegawai['nama_pegawai'];?></td>
                        <td>
                            <a class="btn btn-success" href="../pengembalian/proses_kembali.php?id_peminjaman=<?php echo $data['id_peminjaman'];?>">Kembalikan</a>
                        </td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
           
        </div>
    </div>
</div>
</div>
</div>
 <script src="assets/js/jquery-1.10.2.js"></script>
      <!-- BOOTSTRAP SCRIPTS -->
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- METISMENU SCRIPTS -->
    <script src="assets/js/jquery.metisMenu.js"></script>
     <!-- DATA TABLE SCRIPTS -->
    <script src="assets/js/dataTables/jquery.dataTables.js"></script>
    <script src="assets/js/dataTables/dataTables.bootstrap.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
    </script>
         <!-- CUSTOM SCRIPTS -->
    <script src="assets/js/custom.js"></script>
    
   
</body>
</html>
